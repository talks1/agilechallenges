THere is a cost with the Agile Continuous Delivery approach which is important to appreciate.

With agile continuous delivery there are many transitions from one working state to another working state.

```
working-state => some changes => working-state
```

Because there are many of them there needs to be more emphasis on doing changes efficiently and confidently.

What I have noticed that people tend to want to commit their changes so they dont lose/forget then even if they arent sure they deliver a new working state.  There are a whole lot of commits of partially complete work.


Efficiently in terms of 
- make changes
- verifying the functionality remains working following a change


If this is not done then
- there may be an appearance of speed at the beginning but it is false as additional functionality is weighed down by regression original functionality.
- subsequent changes into risk to working functionality...which creates aversion to change
- its difficult for new people to make changes to the functionality...the number of resources which can work on some functionality is reduced.

So when there is a delivery made there needs to be 
- the delivered functionality
- the ability to know that the delivered functionality remains working after any subsequent change.

For definition of done there needs to be a conscious decision to either
- not include ability to test the functionality as part of build process
- delivery of test capability for the additional functionality
- creation of high priority task to develop that functionlity

QUestion for our team
- ??